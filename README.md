# Ansible RobotFramework

This role install RobotFramework with libraries, plus Selenium, some dumb RobotFramework tests. DISCLAIMER : might not work.

Requirements
------------

Python 3.5 or above (Ansible 2.5 requirement).

Role Variables
--------------

See `defaults/main.yml` and `vars/main.yml`, or the example below

Dependencies
------------

See `meta/main.yml` for other roles dependencies

Example Playbook
----------------

```yaml
---
- hosts: servers
- roles:
  - role: rgarrigue.robotframework

```

Tests
-----

Test are made via Molecule and TestInfra. They will be automatically ran by the CI after every push. For a local run

- Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- Install [Vagrant](https://www.vagrantup.com/downloads.html)
- Open a command prompt in the repository local folder and run `vagrant up`
- Wait for it to be done starting & configuring & provisioning, then `vagrant ssh`
- Once connected in the VM, testing molecule is `cd /vagrant/; molecule test`

One thing specific to this role : we install selenium module, so `molecule dependency` need to be run before `molecule test` otherwise it's missing selenium task definition.

Author Information
------------------

Rémy Garrigue
