import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize("package", [("robotframework")])
def test_pip_package_installed(host, package):
    p = host.pip_package(package)
    assert p.is_installed


@pytest.mark.parametrize("service", [("selenium", "httpd")])
def test_service_is_running(host, service):
    s = host.service(service)
    assert s.is_running
    assert s.is_enabled
