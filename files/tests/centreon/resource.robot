*** Settings ***
Documentation     Resource file for testing Centreon
...               RG              2018/08/17    init
Library           REST
Library           SSHLibrary
Library           CentreonLibrary.py   ${API}

*** Variables ***
${CENTREON_VERSION}   2.9.17
${API}                http://192.168.10.10/api
${USERNAME}           admin
${PASSWORD}           admin
${PLUGIN_DIR}         /usr/lib64/nagios/plugins

*** Keywords ***
Centreon REST authentification
    ${token}=    Authentification   ${USERNAME}  ${PASSWORD}
    Set headers     { "centreon-auth-token": "${token}" }

Centreon REST clapi object action
    [Arguments]   ${OBJECT}   ${ACTION}
    POST          ${API}/index.php?object=centreon_clapi&action=action    { "object": "${OBJECT}", "action": "${ACTION}" }

Centreon REST clapi action values
    [Arguments]   ${ACTION}   ${VALUES}
    POST          ${API}/index.php?object=centreon_clapi&action=action    { "action": "${ACTION}", "values": "${VALUES}" }

Centreon REST apply configuration
    Centreon REST clapi action values   APPLYCFG    central
