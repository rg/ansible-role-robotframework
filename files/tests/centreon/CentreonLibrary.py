#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests

"""This RobotFramework library is just for the thing we can't do directly with RobotFramework libraries, especially RESTinstance
"""


class CockpitLibrary:

    __version__ = "0.1"

    def __init__(self, api):
        self.api = api

    def authentification(self, username="admin", password="admin"):
        payload = (
            '-----1708201817082018\r\nContent-Disposition: form-data; name="username"\r\n\r\n'
            + username
            + '\r\n-----1708201817082018\r\nContent-Disposition: form-data; name="password"\r\n\r\n'
            + password
        )
        headers = {"content-type": "multipart/form-data; boundary=---1708201817082018"}

        response = requests.request(
            "POST",
            self.api + "/index.php?action=authenticate",
            data=payload,
            headers=headers,
        )
        return response.json()["authToken"]
