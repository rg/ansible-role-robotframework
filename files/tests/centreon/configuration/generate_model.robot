*** Settings ***
Resource        ../resource.robot
Suite setup     Centreon REST authentification
Test Template   Generate configuration objects files

*** Test Cases ***    OBJECT
Commands              CMD
Contact Groups        CG
Contact Templates     CONTACTTPL
Contacts              CONTACT
Host Categories       HC
Host Group Services   HGSERVICE
Host Groups           HG
Host Templates        HTPL
Service Categories    SC
Service Groups        SG
Service Templates     STPL
Services              SERVICE
Settings              SETTINGS
Time Periods          TP
Traps                 TRAP

*** Keywords ***
Generate configuration objects files
    [Arguments]   ${OBJECT}
    Centreon REST clapi object action    ${OBJECT}   SHOW
    Output        response body   ${CURDIR}/objects/${COCKPIT_VERSION}/${OBJECT}.json   sort_keys=true
