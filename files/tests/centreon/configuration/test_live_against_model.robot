*** Settings ***
Resource        ../resource.robot
Suite setup     Centreon REST authentification
Test Template   Compare live configuration objects against model

*** Test Cases ***    OBJECT
Commands              CMD
Contact Groups        CG
Contact Templates     CONTACTTPL
Contacts              CONTACT
Host Categories       HC
Host Group Services   HGSERVICE
Host Groups           HG
Host Templates        HTPL
Service Categories    SC
Service Groups        SG
Service Templates     STPL
Services              SERVICE
Settings              SETTINGS
Time Periods          TP
Traps                 TRAP

*** Keywords ***
Compare live configuration objects against model
    [Arguments]   ${OBJECT}
    # Expect response                     ${CURDIR}/objects/${COCKPIT_VERSION}/${OBJECT}.json
    Centreon REST clapi object action     ${OBJECT}         SHOW
    Object                                response body     ${CURDIR}/objects/${COCKPIT_VERSION}/${OBJECT}.json
