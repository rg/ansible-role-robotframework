*** Settings ***
Documentation     A dumb test just to validate ansible-role-robotframework
...               RG              2018/08/17    init
Test Template     Check files exist
Library           OperatingSystem

*** Test Cases ***      PATH
Hosts file              /etc/hosts
Resolv file             /etc/resolv.conf

*** Keywords ***

Check files exist
    [Arguments]         ${PATH}
    File Should Exist   ${PATH}
